var drumButtons = document.querySelectorAll(".drum");

for (var i = 0; i < drumButtons.length; i++) {
  drumButtons[i].addEventListener("click", function() {
    var buttonClicked = this.innerHTML;
    handleClick(buttonClicked);
    buttonAnimation(buttonClicked)
  });
}

document.addEventListener("keydown", function(event) {
  var key = event.key;
  handleKeyDown(key);
  buttonAnimation(key)
});

function handleClick(buttonClicked) {

  var audio;

  switch (buttonClicked) {
    case "w":
      audio = new Audio('sounds/tom-1.mp3');
      audio.play();
      break;
    case "a":
      audio = new Audio('sounds/tom-2.mp3');
      audio.play();
      break;
    case "s":
      audio = new Audio('sounds/tom-3.mp3');
      audio.play();
      break;
    case "d":
      audio = new Audio('sounds/tom-4.mp3');
      audio.play();
      break;
    case "j":
      audio = new Audio('sounds/snare.mp3');
      audio.play();
      break;
    case "k":
      audio = new Audio('sounds/crash.mp3');
      audio.play();
      break;
    case "l":
      audio = new Audio('sounds/kick-bass.mp3');
      audio.play();
      break;
  }
}

function handleKeyDown(keyCode) {
  // var keyCode = event.key;
  var audioID = "";

  switch (keyCode) {
  case "w":
    audio = new Audio('sounds/tom-1.mp3');
    audio.play();
    break;
  case "a":
    audio = new Audio('sounds/tom-2.mp3');
    audio.play();
    break;
  case "s":
    audio = new Audio('sounds/tom-3.mp3');
    audio.play();
    break;
  case "d":
    audio = new Audio('sounds/tom-4.mp3');
    audio.play();
    break;
  case "j":
    audio = new Audio('sounds/snare.mp3');
    audio.play();
    break;
  case "k":
    audio = new Audio('sounds/crash.mp3');
    audio.play();
    break;
  case "l":
    audio = new Audio('sounds/kick-bass.mp3');
    audio.play();
    break;
  }
}

function buttonAnimation(currentKey) {
  var activeButton = document.querySelector("." + currentKey);
  activeButton.classList.add("pressed");

  setTimeout(function() {
    activeButton.classList.remove("pressed");
  }, 100);
}
